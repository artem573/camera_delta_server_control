from vector import Vector
from auxFunc import *
from WiFi import *

class Pole(Connection):
    def __init__(self, name):
        Connection.__init__(self)
        self.name = name
        self.pos = Vector(0,0,0)
        self.l_arm = 0
        self.dldt = 0

    def setup(self, pos, pos_cargo):
        self.pos = pos
        self.l_arm = (pos - pos_cargo).mag()
        self.sendData("#" + str(self.pos.x()) + "#" + str(self.pos.y()) + "#" + str(self.pos.z()) + "#" + str(pos_cargo.x()) + "#" + str(pos_cargo.y()) + "#" + str(pos_cargo.z()))

    def updateLArm(self, l_arm):
        self.l_arm = l_arm

    def updateSpeed(self, pos_cargo, dxdt, dydt, dzdt):
        self.dldt = (
            (pos_cargo.x() - self.pos.x()) * dxdt +
            (pos_cargo.y() - self.pos.y()) * dydt +
            (pos_cargo.z() - self.pos.z()) * dzdt
            ) / self.l_arm;

        self.sendData("{:.3f}".format(self.dldt));

        
