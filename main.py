import sys
from cargo import Cargo
from pole import Pole
from control import Control
from joystick import Joystick


def main():
    cargo = Cargo()
    poleA = Pole("poleA")
    poleB = Pole("poleB")
    joystick = Joystick()
    control = Control(joystick, cargo, poleA, poleB)
    control.setup()
    
    while not control.isFinished():
        control.run()

    sys.exit()



if (__name__ == "__main__"):
    main()
