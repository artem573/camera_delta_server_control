from vector import Vector
from endPoint import *
from WiFi import *


class Control:
    def __init__(self, joystick, cargo, poleA, poleB):
        self.joystick = joystick
        self.cargo = cargo
        self.poleA = poleA
        self.poleB = poleB
        self.break_run = False
        self.tcpserver = TCPServer([joystick, poleA, poleB])
        self.tcpserver.start()
        self.dxdt = 0
        self.dydt = 0
        self.dzdt = 0

        self.DEBUG = True
        self.DXYZDT_MAX = 0.1
       
        

    def setup(self):
        while True:
            if (self.joystick.haveNewData()):
                msg = self.joystick.getData()
                # print("new msg joystick:", msg)
                msg = msg.split("#")
                # print("splitted:", msg)
                if (msg[1] != self.poleA.name):
                    print("Got wrong init msg in Control::setup()", msg[1], "instead of", self.poleA.name)
                pos = msg[2].split(",")
                vecA = Vector(float(pos[0]), float(pos[1]), float(pos[2]))
                
                if (msg[3] != self.poleB.name):
                    print("Got wrong init msg in Control::setup()", msg[3], "instead of", self.poleB.name)
                pos = msg[4].split(",")
                vecB = Vector(float(pos[0]), float(pos[1]), float(pos[2]))

                if (msg[7] != self.cargo.name):
                    print("Got wrong init msg in Control::setup()", msg[7], "instead of", self.cargo.name)
                pos = msg[8].split(",")
                vecCargo = Vector(float(pos[0]), float(pos[1]), float(pos[2]))

                self.poleA.setup(vecA, vecCargo)
                self.poleB.setup(vecB, vecCargo)
                self.cargo.setup(vecCargo, vecA, vecB)
                print("setup was succesful")

                break
        

        

    def run(self):
        if (self.joystick.haveNewData()):
            msg = self.joystick.getData()
            print("new msg joystick:", msg)
            x, y, z = msg.split("#")
            self.dxdt = float(x) * self.DXYZDT_MAX
            self.dydt = float(y) * self.DXYZDT_MAX
            self.dzdt = float(z) * self.DXYZDT_MAX
            self.poleA.updateSpeed(self.cargo.pos, self.dxdt, self.dydt, self.dzdt)
            self.poleB.updateSpeed(self.cargo.pos, self.dxdt, self.dydt, self.dzdt)
                
        if (self.poleA.haveNewData()):
            msg = self.poleA.getData()
            print("new msg poleA:", msg)
            l_arm = float(msg)
            self.poleA.updateLArm(l_arm)
            self.cargo.update_position(self.poleA.l_arm, self.poleB.l_arm)
            if self.DEBUG: self.output()

        if (self.poleB.haveNewData()):
            msg = self.poleB.getData()
            print("new msg poleB:", msg)
            l_arm = float(msg)
            self.poleB.updateLArm(l_arm)
            self.cargo.update_position(self.poleA.l_arm, self.poleB.l_arm)
            if self.DEBUG: self.output()


    def output(self):
        print(self.dxdt, self.dydt, self.dzdt, " / ", self.poleA.l_arm, self.poleB.l_arm, self.cargo.pos)

    def isFinished(self):
        return self.break_run
    
