from enum import Enum

MINMAX_DELTA = 0.1 # [m]

class Activated(Enum):
    LOWER = 0,
    HIGHER = 1


class EndPoint:
    def __init__(self, activated):
        self.activated = activated

    def isActivated(self, point_z):
        pass

    def limit_speed(self, dzdt):
        pass

# ============================================================== #

class EndPointZPlane(EndPoint):
    def __init__(self, activated, z):
        EndPoint.__init__(self, activated)
        self.z = z

        if (activated == Activated.LOWER):
            self.z += MINMAX_DELTA
        else:
            self.z -= MINMAX_DELTA
  
    def isActivated(self, point_z):
        if ((point_z < self.z) and (self.activated == Activated.LOWER)):
            return True
        if ((point_z > self.z) and (self.activated == Activated.HIGHER)):
            return True
        return False

    def limit_speed(self, dzdt):
        if (self.activated == Activated.LOWER):
            return max(dzdt, 0)
        else:
            return min(dzdt, 0)
