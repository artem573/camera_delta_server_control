from vector import Vector

class Data:
  def __init__(self):
    self.POS_A = Vector(0, 0, 1.46)
    self.POS_CARGO = Vector(0, 0, 1.0)
    self.DXYZDT_MAX = 0.1
    self.DEBUG = True
