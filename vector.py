
class Vector():
    def __init__(self, *args):
        if (len(args) == 0): self.values = [0, 0, 0]
        elif (len(args) == 1 and isinstance(args[0], self.__class__)):
            self.values = [i for i in args[0]]
        elif (len(args) == 3): self.values = [i for i in args]
        else: raise NameError("Error in Vector.__init__: args != 0 and != 3")

    def __add__(self, vec):
        if isinstance(vec, self.__class__):
            ret = tuple( a + b for a, b in zip(self, vec))
            return Vector(*ret)
        else:
            raise TypeError("unsupported operand type(s) for +: '{}' and '{}'").format(self.__class__, type(vec))

    def __sub__(self, vec):
        if isinstance(vec, self.__class__):
            ret = tuple( a - b for a, b in zip(self, vec))
            return Vector(*ret)
        else:
            raise TypeError("unsupported operand type(s) for -: '{}' and '{}'").format(self.__class__, type(vec))

    def __mul__(self, a):
        if isinstance(a, float):
            ret = tuple( a*comp for comp in self.values )
            return Vector(*ret)
        else:
            raise TypeError("unsupported operand type(s) for *: '{}' and '{}'").format(self.__class__, type(a))

    def __truediv__(self, a):
        if isinstance(a, float):
            ret = tuple( comp/a for comp in self.values )
            return Vector(*ret)
        else:
            raise TypeError("unsupported operand type(s) for /: '{}' and '{}'").format(self.__class__, type(a))

    def inner(self, vec):
        if isinstance(vec, self.__class__):
            return sum(a * b for a, b in zip(self, vec))
        else:
            raise TypeError("unsupported operand type(s) for inner: '{}' and '{}'").format(self.__class__, type(vec))

    def outer(self, vec):
        if isinstance(vec, self.__class__):
            x = self.y()*vec.z() - self.z()*vec.y()
            y = self.z()*vec.x() - self.x()*vec.z()
            z = self.x()*vec.y() - self.y()*vec.x()
            return Vector(x,y,z)
        else:
            raise TypeError("unsupported operand type(s) for outer: '{}' and '{}'").format(self.__class__, type(vec))
        
    def x(self):
        return self.values[0]

    def y(self):
        return self.values[1]

    def z(self):
        return self.values[2]

    def setx(self, val):
        self.values[0] = val

    def sety(self, val):
        self.values[1] = val

    def setz(self, val):
        self.values[2] = val

    def __str__(self):
        return str(self.values)

    def __iter__(self):
        return self.values.__iter__()

    def mag(self):
        return (sum(a**2 for a in self.values))**0.5
        
    def rotateCCW_xz(self):
        x = self.x()
        self.setx(-self.z())
        self.setz(x)
