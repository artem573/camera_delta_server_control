from vector import Vector

class Cargo:
    def __init__(self):
        self.name = "cargo"
        self.pos = Vector(0,0,0)
        self.vecA = Vector(0,0,0)
        self.vecB = Vector(0,0,0)
        self.set_pos = False

    def setup(self, pos, posA, posB):
        self.pos = pos
        self.vecA = posA
        self.vecB = posB
        self.vecAB = self.vecB - self.vecA
        self.mag_vecAB = self.vecAB.mag()
        self.ex = self.vecAB / self.mag_vecAB
        self.ey = Vector(0,0,0)
        self.ez = Vector(self.ex)
        self.ez.rotateCCW_xz()
        self.set_pos = True
        
    def update_position(self, lA, lB):
        # print(lA, lB)
        xp = (lA**2 - lB**2 + self.mag_vecAB**2) / (2 * self.mag_vecAB)
        yp = 0.0
        zp = -(lA**2 - xp**2)**0.5
        # print(self.ex, xp, self.ey, yp, self.ez, zp)
        vecP = self.vecA + self.ex*xp + self.ey*yp + self.ez*zp
        self.pos = vecP
        # print(vecP)
                                
