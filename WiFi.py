import sys
import socket
from threading import Thread
import queue
from enum import Enum


class ClientType(Enum):
    SENDER = 0,
    RECEIVER = 1
    

class Connection:
    def __init__(self):
        self.q_send = queue.Queue()
        self.q_recv = queue.Queue()

    def setConnection(self, connection_socket, ip, port):
        self.conn_send = ClientThread(connection_socket, ip, port, self.q_send, ClientType.SENDER)
        self.conn_recv = ClientThread(connection_socket, ip, port, self.q_recv, ClientType.RECEIVER)
        self.conn_send.start()
        self.conn_recv.start()

    def haveNewData(self):
        return not self.q_recv.empty()
    
    def getData(self):
        msg = self.q_recv.get()
        try:
            msg = msg.split("\\n")[-2]
        except:
            print("Exception in Connection.getData, wrong format, no escape sequence")
        return msg

    def sendData(self, data):
        self.q_send.put(data)


    
class ClientThread(Thread):
    BUFFER_SIZE = 2048
    
    def __init__(self, client_socket, ip, port, q, type_id):
        Thread.__init__(self)
        self.client_socket = client_socket
        self.ip = ip
        self.port = port
        self.q = q
        self.type_id = type_id
        print("Client connected, ", ip, ":", str(port))

    def run(self):
        if (self.type_id == ClientType.SENDER):
            while True:
                try:
                    if (not self.q.empty()):
                        cmd = self.q.get()
                        self.client_socket.sendall(cmd.encode("utf-8"))
                except:
                    print("Exception in client_socket.sendall(cmd) from " + self.ip)
                    break

        if (self.type_id == ClientType.RECEIVER):
            while True:
                try:
                    data = self.client_socket.recv(ClientThread.BUFFER_SIZE)
                    self.q.put(data.decode("utf-8"))
                except Exception as msg:
                    print("Exception in client_socket.recv() from " + self.ip, msg)
                    break

                if (len(data) == 0):
                    print("len(client_socket.recv()) from " + self.ip + " is 0")
                    break

        self.client_socket.close()
        print("Client disconnected, ", self.ip, ":", str(self.port))



# Multithreaded Python server
class TCPServer(socket.socket):
    TCP_IP = "192.168.0.105"
    # TCP_IP = "127.0.0.1"
    TCP_PORT = 22000
    BUFFER_SIZE = 2084
    MAX_CLIENTS = 4

    def __init__(self, clients):
        socket.socket.__init__(self, socket.AF_INET, socket.SOCK_STREAM)
        self.clients = clients
        self.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            self.bind((TCPServer.TCP_IP, TCPServer.TCP_PORT))
        except socket.error as msg:
            print("Server bind failed", msg[0], msg[1])
            sys.exit()


    def start(self):
        self.listen(TCPServer.MAX_CLIENTS)
        set_clients = 0
        while (set_clients < len(self.clients)):
            print("Waiting for clients")
            (client_socket, (ip, port)) = self.accept()
            name = (client_socket.recv(TCPServer.BUFFER_SIZE)).decode("utf-8")
            found_client = False
            for client in self.clients:
                if (client.name == name):
                    print("Client", name, "connected and set")
                    client.setConnection(client_socket, ip, port)
                    set_clients += 1
                    found_client = True
                    break
            if (not found_client):
                print("Unknown client name:", name)

        

        
def main():
    tcpserver = TCPServer()
    tcpserver.start()

    
if (__name__ == "__main__"):
    main()
